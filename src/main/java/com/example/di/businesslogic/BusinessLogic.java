package com.example.di.businesslogic;

import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.example.di.dto.MessageDTO;

//@RefreshScope // breaks BusinessLogicConfig instantiation by profile as some autoconfiguration tries to instantiate first
@Component
public class BusinessLogic {
    private static Logger log = LoggerFactory.getLogger(BusinessLogic.class);

    public String tier;
    @Value("${app.info.instance_index}")
    private String instanceIndex;

    protected Integer callCount = 0;
    public Integer failOnEverXthCall = 0;

    @Value("${app.businessLogic.sleepMin}")
    public int sleepMin;
    @Value("${app.businessLogic.sleepMax}")
    public int sleepMax;

    //    public BusinessLogic() {
    //        this.tier = "DEFAULT";
    //        this.instanceId = "42";
    //        log.error("DEFAULT constructor of BusinessLogic class called for Autowiring ... should not happen!!!");
    //        try {
    //            throw new Exception();
    //        } catch (Exception ex) {
    //            ex.printStackTrace();
    //        }
    //    }

    public BusinessLogic(String tier, String instanceId) {
        this.tier = tier;
    }

    public MessageDTO transformMessage(MessageDTO payload) throws BusinessException {
        callCount++;
        if ((failOnEverXthCall > 0) && ((callCount % failOnEverXthCall) == 0)) {
            throw new BusinessException(
                    BusinessLogic.class.getSimpleName() + ":" + tier + ":" + instanceIndex + " faild on " + callCount + "th call");
        }
        MessageDTO transformedPayload = new MessageDTO();
        transformedPayload.datetime = payload.datetime;
        transformedPayload.message = payload.message + " transformed by " + tier;

        Random rand = new Random(System.currentTimeMillis());
        long sleeptime = sleepMin + rand.nextInt(sleepMax - sleepMin);
        try {
            log.info("[{}] {} sleep ms:{}", instanceIndex, tier, sleeptime);
            Thread.sleep(sleeptime);
        } catch (InterruptedException e) {
            log.error("[{}] {} Thread.sleep: {}", instanceIndex, tier, e.getMessage());
        }

        return transformedPayload;
    }

}
