package com.example.di;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.example.di.businesslogic.BusinessLogic;

@SpringBootApplication
public class DiApplication implements CommandLineRunner {

    @Autowired
    private BusinessLogic businessLogic;

    public static void main(String[] args) {
        SpringApplication.run(DiApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        System.out.println("===================================================");
        System.out.println("====        BusinessLogic: " + businessLogic.tier + "             ====");
        System.out.println("===================================================");
    }
}
