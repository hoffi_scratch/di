package com.example.di.dto;

public class MessageDTO {
    public String datetime = "uninitialized";
    public String message = "NONE";
    public String modifier = "";

    @Override
    public String toString() {
        return String.format("%s MESSAGE: '%s' modifiers: '%s'", datetime, message, modifier);
    }
}
